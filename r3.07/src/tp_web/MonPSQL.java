 package tp_web;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;

public class MonPSQL {
	public static void main(String[] args) {
		Properties p = new Properties();
		try {
			p.load(new FileInputStream("config.prop"));
		} catch(IOException e) {
			e.printStackTrace();
		}
		String url = p.getProperty("url");
		String login = p.getProperty("login");
		String password = p.getProperty("password");
		String driver = p.getProperty("driver");
		Connection con = null;
		try(Scanner scanner = new Scanner(System.in)){
			Class.forName(driver);
			con = DriverManager.getConnection(url,login,password);
			Statement stmt = con.createStatement();
			String query = "";
			System.out.print("Saisir un champ\n=>");
			String sc = scanner.nextLine();
			while(!sc.startsWith("\\q")) {
				System.out.print("=>");
				sc = scanner.nextLine();
				ResultSet rs = null;
				if(!sc.startsWith("\\i") && !sc.startsWith("\\d")) {
					query = sc;
					rs = stmt.executeQuery(query);
				}
				while(rs.next()) {
					System.out.println(rs.getString("nom"));
				}
			}
		}catch (Exception e){ System.out.println(e.getMessage());}}
}
