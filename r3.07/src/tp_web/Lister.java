package tp_web;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;

public class Lister {
	public static void main(String args[]) throws Exception {
		Properties p = new Properties();
		try {
			p.load(new FileInputStream("config.prop"));
		} catch(IOException e) {
			e.printStackTrace();
		}
		String url = p.getProperty("url");
		String login = p.getProperty("login");
		String password = p.getProperty("password");
		String driver = p.getProperty("driver");
		Connection con = null;

		try{
			Class.forName(driver);
			con = DriverManager.getConnection(url,login,password);
			String table = args[0];
			String query = "select * from " + table + ";";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			int nbCol = rs.getMetaData().getColumnCount();
			String column = "";
			for(int i = 1 ; i <= nbCol ; i++) {
				column += rs.getMetaData().getColumnName(i) + " ";
			}
			String info = "";
			while(rs.next()) {
				for(int i = 1 ; i <= nbCol ; i++) {
					info += rs.getString(i) + " ";
				}	
				info += "\n";
			}
			System.out.println(column);
			System.out.println(info);

		}catch (Exception e){ System.out.println(e.getMessage());
		}finally{
			try {con.close();} catch(Exception e2) {}
		}		
	}
}
