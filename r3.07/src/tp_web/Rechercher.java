package tp_web;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.Scanner;

public class Rechercher {
	public static void main(String args[]) throws Exception {
		Properties p = new Properties();
		try {
			p.load(new FileInputStream("config.prop"));
		} catch(IOException e) {
			e.printStackTrace();
		}
		String url = p.getProperty("url");
		String login = p.getProperty("login");
		String password = p.getProperty("password");
		String driver = p.getProperty("driver");
		Connection con = null;

		try{
			Class.forName(driver);
			con = DriverManager.getConnection(url,login,password);
			System.out.println("Saisir un nom : ");
			Scanner saisieUser = new Scanner(System.in);
			PreparedStatement pstmt = con.prepareStatement("select NOM,PRENOM,AGE from CLIENTS WHERE NOM=?");
			pstmt.setString(1, saisieUser.next());
			ResultSet rs = pstmt.executeQuery();
			if(rs.next()) {
				System.out.println("Informations de l'abonné:");
				System.out.println(rs.getString("nom") + rs.getString("prenom") + rs.getInt("age"));

			}else {
				System.out.println("Utilisateur inconnu");
			}

		}catch (Exception e){ System.out.println(e.getMessage());
		}finally{
			try {con.close();} catch(Exception e2) {}
		}		
	}
}
