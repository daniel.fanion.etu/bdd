package tp_web;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import org.postgresql.util.PSQLException;

public class Transac2 {
	public static void main(String args[]) throws Exception {
		Properties p = new Properties();
		try {
			p.load(new FileInputStream("config.prop"));
		} catch(IOException e) {
			e.printStackTrace();
		}
		String url = p.getProperty("url");
		String login = p.getProperty("login");
		String password = p.getProperty("password");
		String driver = p.getProperty("driver");
		Connection con = null;

		try{
			Class.forName(driver);
			con = DriverManager.getConnection(url,login,password);
			Statement st = con.createStatement();
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);
			String pers = args[0];
			String prod = args[1];
			String r1 = "select qute from produit where prod='" + prod + "'";
			ResultSet rs = st.executeQuery(r1);
			rs.next();
			if (rs.getInt("qute") > 0) {
				String r3 = "update compte set solde=solde+100 where pers='" + pers + "'";
				st.executeUpdate(r3);
				Thread.sleep(5000);
				String r2 = "update produit set qute=qute-1 where prod='" + prod + "'";
				st.executeUpdate(r2);
				System.out.println("Achat effectué");
				
			} else {
				System.out.println("Achat impossible");
				
			}
			con.commit();
		}catch (PSQLException e){ 
			con.rollback();
			System.out.println("rollback");;
		}finally{
			try {con.close();} catch(Exception e2) {}
		}		
	}
}