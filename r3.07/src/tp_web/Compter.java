package tp_web;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

public class Compter {
	public static void main(String args[]) throws Exception {
		Properties p = new Properties();
		try {
			p.load(new FileInputStream("config.prop"));
		} catch(IOException e) {
			e.printStackTrace();
		}
		String url = p.getProperty("url");
		String login = p.getProperty("login");
		String password = p.getProperty("password");
		String driver = p.getProperty("driver");

		Connection con = null;
		try{
			Class.forName(driver);
			con = DriverManager.getConnection(url,login,password);
			Statement stmt = con.createStatement();
			String query = "select Count(*) as c from CLIENTS";
			ResultSet rs = stmt.executeQuery(query);
			System.out.println("Nombre de clients:");
			rs.next();
			System.out.println(rs.getString(1));

		}catch (Exception e){ System.out.println(e.getMessage());
		}finally{
			try {con.close();} catch(Exception e2) {}
		}		
	}
}
