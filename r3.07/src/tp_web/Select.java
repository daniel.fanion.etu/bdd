package tp_web;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

public class Select {
	public static void main(String args[]) throws Exception {
		Properties p = new Properties();
		try {
			p.load(new FileInputStream("config.prop"));
		} catch(IOException e) {
			e.printStackTrace();
		}
		String url = p.getProperty("url");
		String login = p.getProperty("login");
		String password = p.getProperty("password");
		String driver = p.getProperty("driver");
		Connection con = null;
		try{
			Class.forName(driver);
			con = DriverManager.getConnection(url,login,password);
			Statement stmt = con.createStatement();
			String query = "select NOM,PRENOM,AGE from CLIENTS";
			ResultSet rs = stmt.executeQuery(query);
			System.out.println("Liste des clients:");
			while (rs.next())
			{
				String n = rs.getString("nom"); // col 1
				String p2 = rs.getString("prenom"); // col 2
				int a = rs.getInt("age"); // col 3
				System.out.println(n + " " + p2 + " " + a);
			}


		}catch (Exception e){ System.out.println(e.getMessage());
		}finally{
			try {con.close();} catch(Exception e2) {}
		}		
	}
}
