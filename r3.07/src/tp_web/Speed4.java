package tp_web;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Properties;

public class Speed4 {
	public static void main(String args[]) throws Exception {
		Properties p = new Properties();
		try {
			p.load(new FileInputStream("config.prop"));
		} catch(IOException e) {
			e.printStackTrace();
		}
		String url = p.getProperty("url");
		String login = p.getProperty("login");
		String password = p.getProperty("password");
		String driver = p.getProperty("driver");
		Connection con = null;
		int cpt1 = (int) System.currentTimeMillis();
		try{
			Class.forName(driver);
			con = DriverManager.getConnection(url,login,password);
			Statement stmt = con.createStatement();
			stmt.executeUpdate("DELETE from Clients;");
			String query = "insert into Clients(nom,prenom,age)" +"values(?,?,?);" ;
			PreparedStatement ps1 = con.prepareStatement(query);
			
			for (int i = 0 ; i<10000 ; i++ ) {

				ps1.setString(1,"nom"+i);
				ps1.setString(2,"prenom"+i);
				ps1.setInt(3,18);
				ps1.addBatch();
			}
			stmt.executeBatch();
			int cpt2 = (int) System.currentTimeMillis();
			System.out.println((double)(cpt2-cpt1)/1000);
		}catch (Exception e){ System.out.println(e.getMessage());
		}finally{
			try {con.close();} catch(Exception e2) {}
		}		
	}
}
