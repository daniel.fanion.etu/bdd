package tp_web;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DS {

	public Connection getConnection() throws SQLException{
		Properties p = new Properties();
		try {
			p.load(new FileInputStream("config.prop"));
		}
		catch(IOException e) {e.printStackTrace();}

		String url = p.getProperty("url");
		String login = p.getProperty("login");
		String password = p.getProperty("password");
		String driver = p.getProperty("driver");

		Connection con = DriverManager.getConnection(url,login,password);
		return con;
	}
	
}
