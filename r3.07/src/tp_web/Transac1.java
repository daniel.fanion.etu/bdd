package tp_web;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Properties;

public class Transac1 {
	public static void main(String args[]) throws Exception {
		Properties p = new Properties();
		try {
			p.load(new FileInputStream("config.prop"));
		} catch(IOException e) {
			e.printStackTrace();
		}
		String url = p.getProperty("url");
		String login = p.getProperty("login");
		String password = p.getProperty("password");
		String driver = p.getProperty("driver");
		Connection con = null;
		
		try{
			Class.forName(driver);
			con = DriverManager.getConnection(url,login,password);
			Statement stmt = con.createStatement();
			con.setAutoCommit(false);
			stmt.executeUpdate("truncate table client;");
			for(int i = 1 ; i <= 1000 ; i++) {
				stmt.executeUpdate("insert into client(cno,nom,prenom,age) " + 
						"values(" + i + ",'nom" + i + "','prenom" + i + "',18);");
			}
			for(int i = 3000 ; i <= 4000 ; i++) {
				stmt.executeUpdate("insert into client(cno,nom,prenom,age) " + 
						"values(" + i + ",'nom" + i + "','prenom" + i + "',18);");
			}
			con.commit();
		}catch (Exception e){ 
			con.rollback();
			System.out.println(e.getMessage());
		}finally{
			try {con.close();} catch(Exception e2) {}
		}		
	}
}

